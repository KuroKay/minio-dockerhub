# Pull l'image minio
FROM minio/mc
#Variable
ENV S3_URL_BACKEUP="url"
ENV S3_ACCESS_TOKEN="token"
ENV S3_KEY="key"
EXPOSE 9001
RUN \
    mc mb minioTim/backup && \
    mc cp -r 5ika minioTim/backup
CMD mc config host add minioTim ${S3_URL} ${S3_ACCESS_TOKEN} ${S3_ACCESS_KEY} --api S3v4
