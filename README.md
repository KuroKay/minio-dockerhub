# Minio-Dockerhub
Travail TP S3 - rendu 12 mai - cours environnement

## Partie 1 - Script Bash:

Création d'un script bash pour backuper sur un serveur S3 (minio dans ce cas) en passant en variables les accès fournis

###1. Installation Minio

L'installation a été faite via Homebrew dans mon cas
brew install minio/stable/mc

###2. Connection au serveur:

```bash
mc config host add ALIAS_DU_SERVEUR $urlMinio $accessKey $secretKey --api S3v4
export accessKey=MY_ACCESS_KEY
export secretKey=MY_SECRET_KEY
export urlMinio=MY_URL
```

###3. Creation Bucket (sorte de dossier/repertoire):

```bash
mc mb ALIAS_DU_SERVEUR/NOM_DU_BUCKET
```

###4. Copie du fichier dans le bucket.

```bash
mc cp -r monfichier.txt ALIAS_DU_SERVEUR/NOM_DU_BUCKET
```


# Partie 2 - Dockerfile - Docker Hub

###1. Création Dockerfile et Puller sur DockerHub

```Dockerfile
FROM minio/mc
ENV S3_URL=""
ENV S3_ACCESS_TOKEN=""
ENV S3_ACCESS_KEY=""
```

###2. Integrer les commandes

```Dockerfile
RUN \
    mc mb minioTim/backup && \
    mc cp -r 5ika minioTim/backup
# Lancer Docker
CMD mc config host add minioTim ${S3_URL_BACKEUP} ${S3_ACCESS_TOKEN} ${S3_KEY} --api S3v4
```